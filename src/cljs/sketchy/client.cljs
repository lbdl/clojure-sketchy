(ns sketchy.client
  (:require [dommy.core :as d]
            [chord.client :refer [ws-ch]]
            [cljs.core.async :refer [<! >! put! close! chan map<]]
            [cljs.reader :refer [read-string]])
  (:require-macros [dommy.macros :refer [node sel1]]
                   [cljs.core.async.macros :refer [go go-loop]]))


(defn events [el type ch]
    (d/listen! el type
               (fn [e]
                 (put! ch e)
                 (.preventDefault e)))
    ch)


(def channel1 (chan))
(def channel2 (chan))
(def channel3 (chan))

(defn greet [name]
  (str "Hello " name))


;;(.addEventListener js/window "mousemove"
;;                  (fn [e]
                    ;(.log js/console e)
;;                    (go
;;                      (>! my-chan e))))

(defn e->v [e]
  [(.-x e) (.-y e)])


;;(go-loop []
;;         (when-let [msg (<! my-chan)]
;;           (.log js/console msg)
;;           (recur)))


(defn draw-point [canvas [x y]]
  (let [context (.getContext canvas "2d")]
    (.fillRect context x y 10 10)))


;;(go-loop []
;;         (when-let [msg (<! my-chan)]
;;          (.log js/console (pr-str [(.-x msg) (.-y msg)]))
;;           (recur)))


;;(go-loop []
;;         (when-let [msg (<! my-chan)]
;;           (.log js/console (pr-str [(.-x msg) (.-y msg)]))
;;           (draw-point [(.-x msg) (.-y msg)])
;;           (recur)))


(defn main []
  (let [canvas (sel1 :#canvas)
        move (map< e->v (events canvas "mousemove" channel1))]
    (go-loop []
             (when-let [msg (<! channel1)]
               ;(.log js/console (pr-str [(.-x msg) (.-y msg)]))
               (draw-point canvas [(.-x msg) (.-y msg)])
               (recur)))))

(set! (.-onload js/window) main)
